const express = require('express');
const mongoose = require('mongoose')

const app = express();
mongoose.connect('mongodb+srv://alineraiza:alineraiza10@cluster0-4rbfj.mongodb.net/week10?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
app.use(express.json());


//metodos http: GET= buscar dados, POST= inserir dados, PUT= atualizar, altera dados, DELETE= excluir
// tipos de parâmetros
// Query params - request.query filtros, ordenação, paginação. 
// Route params - identificar um recurso na alteração ou remoção. 
// Body request.body - dados para criação ou alteração de um registro.
app.get('/', (request, response) => {
    console.log(request.query)
    return response.json({ mensagem: 'exemplo GET', nome: 'com query' })
});

app.get('/users', (request, response) => {
    console.log(request.query)
    return response.json({ mensagem: 'exemplo GET', nome: 'com query' })
});

app.put('/users/:id', (request, response) => {
    console.log(request.params)
    return response.json({ mensagem: 'exemplo PUT com route params' })
});

app.post('/users', (request, response) => {
    console.log(request.body)
})

app.listen(3333);